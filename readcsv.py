import csv
import logging

def lectureCSV(nomCSV, delimiteur=";"):
    """
    Lire le contenu d'un fichier.

    :param nomFichier: nom du fichier à traiter
    :param delimiteur: délimiteur qui sépare les valeurs du fichier CSV
    :return: contenu du fichier sous forme de tableau
    """
    fileContent = ""
    logging.debug("Début de la lecture du fichier")
    with open(nomCSV) as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiteur)
        fileContent = [row for row in reader]
        logging.info("Fichier lu avec succès")
        logging.debug("Fin de la lecture du fichier")
    return fileContent